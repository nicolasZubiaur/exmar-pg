/**
 * Created by madam on 2/1/16.
 */
/**
 * Created by madam on 1/22/16.
 */

 function start(){
    $("#saveload").css('display','none');
    $("#state").css('display','none');

        var resultado = JSON.parse(window.localStorage["travel_main"]);

        var str="";
       
        for (var i=0;i<resultado.as_user.length;i++)
        {
            var itinerary_id=resultado.as_user[i].id;
            var name=resultado.as_user[i].name;


            var res1 =resultado.as_user[i].start.split('-');
            var res2 =resultado.as_user[i].end.split('-');
            var res3=res1[2]+"/"+res1[1]+"/"+res1[0] + ' - ' + res2[2]+"/"+res2[1]+"/"+res2[0];


            str+='<a href="travel_list.html?itinerary_id='+itinerary_id+'"><div class="border1">'+
        '<div class="lineborder-1"></div>'+
            '<div class="tablecell" >'+
            '<div class="table1">'+ resultado.as_user[i].name+ '</div>'+
            '<div class="table2">'+res3+
            '</div>'+
            '<div class="table3">'+resultado.as_user[i].days + " días"+
            '</div>'+
            '</div>'+
            '<div class="rightside-image">'+
            '<img src="images/rightside.png" style="width: 30px">'+
            '</div>'+
            '</div></a>';
        }
        $('#material-container').append(str);


        var group="";

        for (var i=0;i<resultado.as_group.length;i++)
        {
            //alert(JSON.stringify(resultado.as_group.length));
            var itinerary_id= resultado.as_group[i].id;
            var group_name=resultado.as_group[i].name;
            var group1 =resultado.as_group[i].start.split('-');
            var group2 =resultado.as_group[i].end.split('-');
            var group3=group1[0]+"/"+group1[1]+"/"+group1[2] + ' - ' + group2[0]+"/"+group2[1]+"/"+group2[2];

            group+='<a href="travel_list.html?itinerary_id='+itinerary_id+'"><div class="border1">'+
            '<div class="lineborder-1"></div>'+
            '<div class="tablecell" >'+
            '<div class="table1">'+ resultado.as_group[i].name+ '</div>'+
            '<div class="table2">'+group3+
            '</div>'+
            '<div class="table3">'+resultado.as_group[i].days + " días"+
            '</div>'+
            '</div>'+
            '<div class="rightside-image">'+
            '<img src="images/rightside.png" style="width: 30px">'+
            '</div>'+
            '</div></a>';

        }
        $('#material-container').append(group);
 }

$(document).ready(function() {

    var post_token =  window.localStorage["token"];
    var post_userid = window.localStorage["user_id"];
    var active = window.localStorage["active"];

    if(active == 'inactive'){
        var url = 'http://turismoexmar.com/api/itineraries/index/'+post_userid;
        $('#text-align').text('Mi Historial de Viajes');
    }else{
         var url = 'http://turismoexmar.com/api/itineraries/index/'+post_userid+'/active';
         $('#text-align').text('Mis Viajes Activos');
    }

    $('#active').click(function(){
        window.localStorage["active"] = 'active';
        location.reload();
    });

    $('#inactive').click(function(){
        window.localStorage["active"] = 'inactive';
        location.reload();
    });

    if(window.localStorage["offline"]=='Off'){
        $.ajax({
            method: "GET",
            url: url,
            dataType: "json",
            context: document.body, beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + post_token);}
        }).done(function(result) {
            window.localStorage["travel_main"]=JSON.stringify(result.data);
            start();

        }).fail(function(result){
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            var jsonr = JSON.parse(result.responseText);
            if(jsonr.success==false){
                if(jsonr.data.message='Expired token'){
                    navigator.notification.alert('Favor de volver a ingresar tus credenciales');
                    window.localStorage["token"]='';
                    location.href="index.html";
                }else{
                    navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
                }
            }else{
                navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
            }
        });
    }else{
        $("#saveload").css('display','none');
        $("#state").css('display','none');
        if(window.localStorage["travel_main"]==''){
            $('#alertoffline').show(); 
        }else if(!window.localStorage["travel_main"]){
            $('#alertoffline').show();  
        }else{
            $("#saveload").css('display','block');
            $("#state").css('display','block');
            start();
        }
    }

});

