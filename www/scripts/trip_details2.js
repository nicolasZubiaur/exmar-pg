
/**
 * Created by madam on 2/9/16.
 */

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function getDate(start_date){

    var first_date=start_date;
    var second_date = first_date.split("-");
    var stringToParse=second_date[0]+"/"+second_date[1]+"/"+second_date[2];
    var dateString    = stringToParse.match(/\d{4}\/\d{2}\/\d{2}\s+\d{2}:\d{2}/);
    var dt  = new Date(dateString);
    var dr = dt.toString();
    var q = dr.split("GMT")[0];
    var w=q.split(":");
    var e=w[0]+":"+w[2];
    var r= e.split(" ");
    var list_date= "  "+r[0]+" "+r[2]+" "+r[1]+" "+r[3]+" "+r[4];
    return list_date;
}

function start(){
    $("#saveload").css('display','none');
    $("#state").css('display','none');

        var end_f;
        var arrivalcity;
        var reservation;
        var comments;
        var userslist="";
        var groups="";
        var weather="";
        var weather2="";
        var currency="";

        var resultado = JSON.parse(window.localStorage["result_details2"+getParameterByName("destination_id")]);

        for (var i=0;i<resultado.destination.length;i++)
        {

            end_f=resultado.destination[i].arrival_date;
            arrivalcity=resultado.destination[i].departure_city_name+" <i class='fa fa-arrow-right'></i> "+resultado.destination[i].arrival_city_name;
            reservation=resultado.destination[i].reservation;
            comments=resultado.destination[i].comments;
        }

        for (var i=0;i<resultado.users.length;i++)
        {
            var  users=resultado.users[i].user;
            userslist+='<a><div class="tripdetail_cell2">'+users+
            '</div><a>';
        }
        for (var i=0;i<resultado.groups.length;i++)
        {

            var grouplist=resultado.groups[i].user;
            groups+='<a><div class="tripdetail_cell2">'+grouplist+
            '</div><a>';
        }

        for (var i=0;i<resultado.weather.length;i++)
        {
            c = (resultado.weather[i].temp_departure-32)*0.5556;
            weather = '<div class="tripdetail_weather gradeh">';
            weather +='<p>Clima actual en: '+resultado.destination[i].departure_city_name+'</p>';
            weather += '<div><img src="http://l.yimg.com/a/i/us/we/52/'+resultado.weather[i].code_departure+'.gif"/></div>'
            weather += '<p>'+Math.round(c)+'° C, '+resultado.weather[i].text_departure+'</p>';
            weather += '</div>';

            c2 = (resultado.weather[i].temp_arrival-32)*0.5556;
            weather2 = '<div class="tripdetail_weather gradeh">';
            weather2 +='<p>Clima actual en: '+resultado.destination[i].arrival_city_name+'</p>';
            weather2 += '<div><img src="http://l.yimg.com/a/i/us/we/52/'+resultado.weather[i].code_arrival+'.gif"/></div>'
            weather2 += '<p>'+Math.round(c)+'° C, '+resultado.weather[i].text_arrival+'</p>';
            weather2 += '</div>';
            
        }

        if(resultado.currency!=''){
            currency = '<div class="tripdetail_curren">';
            currency +='<p>'+resultado.currency.to.col0+' <i class="fa fa-arrow-right"></i> '+resultado.currency.from+'</p>';
            currency += '<p><i class="fa fa-money"></i> '+Math.round(resultado.currency.to.col1*10000)/10000+' '+resultado.currency.to.col0+'</p>';
            currency += '</div>';
        }

        var end=getDate(end_f);

        $("#preloader").css('display','none');
        $("#status").css('display','none');
        $('#end_date').html(end);
        $('#arrival_name').html(arrivalcity);
        $('#reservation').html(reservation);
        $('#comments').html(comments);
        $('#users_list').append(userslist);
        $('#group_list').append(groups);
        $('#weather').append(weather2);
        $('#weather2').append(weather);
        $('#currency').append(currency);
}

$(document).ready(function() {
    var post_token =  window.localStorage["token"];
    var destination_id=getParameterByName("destination_id");

    if(window.localStorage["offline"]=='Off'){

        $.ajax({
            method: "GET",
            url: "http://turismoexmar.com/api/itineraries_cities/details/" + destination_id,
            dataType: "json",
            context: document.body, beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + post_token);
            }
        }).done(function (result) {
            window.localStorage["result_details2"+destination_id] = JSON.stringify(result.data);
            start();
        }).fail(function(result){

            $("#saveload").css('display','none');
            $("#state").css('display','none');
            var jsonr = JSON.parse(result.responseText);
            if(jsonr.success==false){
                if(jsonr.data.message='Expired token'){
                    navigator.notification.alert('Favor de volver a ingresar tus credenciales');
                    window.localStorage["token"]='';
                    location.href="index.html";
                }else{
                    navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
                }
            }else{
                navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
            }
        });
    }else{
        $("#saveload").css('display','none');
        $("#state").css('display','none');
        if(window.localStorage["result_details2"+destination_id]==''){
           $('#alertoffline').show(); 
        }else if(!window.localStorage["result_details2"+destination_id]){
            $('#alertoffline').show();
        }else{
            $("#saveload").css('display','block');
            $("#state").css('display','block');
            var resultado = JSON.parse(window.localStorage["result_details2"+destination_id]);
            for (var i=0;i<resultado.destination.length;i++)
            {
                if(destination_id==resultado.destination[i].id){
                    start();
                }else{
                    $('#alertoffline').show();   
                }
            }
        }
    }

    $('#back').click(function(){
        if(device.platform === "iOS" && parseInt(device.version) === 9){
           var resultado = JSON.parse(window.localStorage["result_details2"+destination_id]);
            for (var i=0;i<resultado.destination.length;i++)
            {
                itinerary = resultado.destination[i].itinerary_id;
            }
            location.href="travel_list.html?itinerary_id="+itinerary;
        }else{
            navigator.app.backHistory();
        }
    });
    
});
