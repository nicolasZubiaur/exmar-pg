/**
 * Created by madam on 1/22/16.
 */


$(document).ready(function() {
    $("#saveload").css('display','none');
    $("#state").css('display','none');

    if(!window.localStorage["offline"]){
        window.localStorage["offline"]='On';
    }
    if(!window.localStorage["token"]){
        window.localStorage["offline"]='Off';
    }else{
        location.href="travel_main.html";
    }

    $("#login_button").click(function () {
        $("#saveload").css('display','block');
        $("#state").css('display','block');
        var userid=$('#username').val();
        var password=$('#password').val();

        $.ajax({
            method: "POST",
            url: "http://turismoexmar.com/api/users/token",
            data:{"email": userid,"password": password},
            dataType:"json"
        }).done(function(result) {

            $("#saveload").css('display','block');
            $("#state").css('display','block');


            window.localStorage["token"]= result.data.token;
            window.localStorage["user_id"]= result.data.user_id;


            window.localStorage["username"]= userid;
            window.localStorage["password"]= password;

            $('#username').val(window.localStorage["user_email"]);
            $('#password').val(window.localStorage["password"]);


            location.href="travel_main.html";

        }).fail(function(result){

                $("#saveload").css('display','none');
                $("#state").css('display','none');
                navigator.notification.alert('Nombre de usuario o contraseña no válidos',null,'error','OK');
        });
        
    });

    $("#forgot_button").click(function () {
         $("#saveload").css('display','block');
         $("#state").css('display','block');
        var userid=$('#username').val();
        if(userid!=''){
            $.ajax({
            method: "POST",
            url: "http://turismoexmar.com/api/users/forgot_password/"+userid,
            dataType:"json"
            }).done(function(result) {

                $("#saveload").css('display','block');
                $("#state").css('display','block');
                if(result.data.user!=''){
                    navigator.notification.alert('Se te ha enviado un correo con las instrucciones para recuperar tu contraseña.',null,'aviso','OK');
                    location.href="index.html";
                }else{
                    navigator.notification.alert('Usuario no encontrado',null,'error','OK');
                }
                $("#saveload").css('display','none');
                $("#state").css('display','none');

            }).fail(function(result){

                    $("#saveload").css('display','none');
                    $("#state").css('display','none');
                    navigator.notification.alert('Ocurrió un error, intenta nuevamente',null,'error','OK');
            });
        }else{
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            navigator.notification.alert('Introduce un correo',null,'error','OK');
        }


    });

    $("#forgot_back").click(function () {
        location.href="index.html";
    }); 
});

