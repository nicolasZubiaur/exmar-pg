/**
 * Created by madam on 2/23/16.
 */
/**
 * Created by madam on 2/9/16.
 */

function start(){
    $("#saveload").css('display','none');
        $("#state").css('display','none');

        var name;
        var second_name;
        var father_name;
        var mother_name;
        var email;
        var telephone;

        var resultado = JSON.parse(window.localStorage["contact"]);

        for (var i = 0; i < resultado.agent.length; i++) {
            name = resultado.agent[i].name;
            second_name = resultado.agent[i].second_name;
            father_name = resultado.agent[i].father_lastname;
            mother_name = resultado.agent[i].mother_lastname;
            email = resultado.agent[i].email;
            telephone = resultado.agent[i].telephone;
        }
        /*$("#call").click(function () {
            window.location.href = "tel:" + telephone;
        });*/

        $("#call").attr('href',"tel:+" + telephone);
        $("#telenumber").attr('href',"tel:+" + telephone);
        //alert($("#call").attr('href'));

        $('#agente_name').html("Agente: " + name + " " + second_name + " " + father_name + " " + mother_name);
        $('#telenumber').html(telephone);
        $('#email').html(email);
}

$(document).ready(function () {

    var post_token = window.localStorage["token"];
    var user_id = window.localStorage["user_id"];

    if(window.localStorage["offline"]=='Off'){
        $.ajax({
            method: "GET",
            url: "http://turismoexmar.com/api/users/contact/" + user_id,
            dataType: "json",
            context: document.body, beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + post_token);
            }
        }).done(function (result) {
            window.localStorage["contact"]=JSON.stringify(result.data);
            start();

        }).fail(function(result){
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            var jsonr = JSON.parse(result.responseText);
            if(jsonr.success==false){
                if(jsonr.data.message='Expired token'){
                    navigator.notification.alert('Favor de volver a ingresar tus credenciales');
                    window.localStorage["token"]='';
                    location.href="index.html";
                }else{
                    navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
                }
            }else{
                navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
            }
            
        });
    }else{
        $("#saveload").css('display','none');
        $("#state").css('display','none');
        if(window.localStorage["contact"]==''){
            $('#alertoffline').show();
        }else if(!window.localStorage["contact"]){
            $('#alertoffline').show(); 
        }else{
            $("#saveload").css('display','block');
            $("#state").css('display','block');
            start();
        }
    }


    $("#message_button").click(function () {

        $("#saveload").css('display','block');
        $("#state").css('display','block');

        if($('#contactEmailField').val()==''){
            navigator.notification.alert('Ingresa un título',null,'error','OK');
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            return false;
        }

        if($('#contactMessageTextarea').val()==''){
            navigator.notification.alert('Ingresa un texto',null,'error','OK');
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            return false;
        }
        
        var mensaje = $('#contactMessageTextarea').val();
        var titulo = $('#send_email').val() + ': '+ $('#contactEmailField').val();
        
        $.ajax({
            method: "GET",
            url: 'http://turismoexmar.com/api/users/contactsendmail/' + user_id+'/'+mensaje+'/'+titulo,
            dataType: "json",
            context: document.body, beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + post_token);
            }
        }).done(function (result) {
            $("#saveload").css('display','none');
            $("#state").css('display','none');
            navigator.notification.alert('Tu mensaje se ha enviado!',null,'Aviso','OK');
            $('#contactMessageTextarea').val('')
            $('#contactEmailField').val('')

        });
        
    });

});
