/**
 * Created by madam on 2/17/16.
 */
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getDate(start_date){

    var first_date=start_date;
    var second_date = first_date.split("-");
    var stringToParse=second_date[0]+"/"+second_date[1]+"/"+second_date[2];
    var dateString    = stringToParse.match(/\d{4}\/\d{2}\/\d{2}\s+\d{2}:\d{2}/);
    var dt  = new Date(dateString);
    var dr = dt.toString();
    var q = dr.split("GMT")[0];
    var w=q.split(":");
    var e=w[0]+":"+w[2];
    var r= e.split(" ");
    var list_date= "  "+r[0]+" "+r[2]+" "+r[1]+" "+r[3]+" "+r[4];
    return list_date;
}
function gethour(start_hours){

    var hour1 = start_hours;
    var hour2 = hour1.split(":");

    var hour = hour2[0] + ":" + hour2[1];
    return hour;
}

var map;
function initialize() {
    var x=parseFloat(lat);
    var y=parseFloat(long);

    var bangalore = { lat:x, lng:y };
    map = new google.maps.Map(document.getElementById('map_canvas'), {
    zoom: 17,
    center: bangalore
    });


 addMarker(bangalore, map);
 }
 function addMarker(location, map) {

 var marker = new google.maps.Marker({
 position: location,
 map: map
 });
 }

function start(){

                $("#saveload").css('display','none');
                $("#state").css('display','none');
               
                var date;
                var name;
                var comments;
                var nights;
                var reservation;
                var hour;
                var services="";
                var users="";
                var weather="";
                var currency="";

                var resultado = JSON.parse(window.localStorage["result_details3"+getParameterByName("location_id")]);
                
                for (var i=0;i<resultado.activities.length;i++)
                {

                    var start=(getDate(resultado.activities[i].start));

                    var end=getDate(resultado.activities[i].end);

                    date = start+" - "+end;

                    comments = resultado.activities[i].comments;

                    nights = resultado.activities[i].nigths;

                    reservation = resultado.activities[i].reservation;

                    name=resultado.activities[i].name;

                    lat=resultado.activities[i].latitude;
                    long=resultado.activities[i].longitude;

                    initialize();
                    var latlong = lat+','+long;
                    
                    $('#gotomaps').attr('onclick',"window.open('http://maps.google.com/?q="+latlong+"', '_system')");

                    if(resultado.activities[i].location_type_id==1){
                        if((resultado.activities[i].check_in!=' ')&&(resultado.activities[i].check_out != ' ')){
                        var check_in = gethour(resultado.activities[i].check_in);

                        var check_out = gethour(resultado.activities[i].check_out);
                        hour = '<div class="tripdetail_cell1">Horarios:</div>';
                        hour += '<div class="tripdetail_cell2">Check-in: '+check_in+'</div>';
                        hour += '<div class="tripdetail_cell2">Check-out: '+check_out+'</div>';
                        }else{
                            hour = '';
                        } 
                    }else{
                        if(resultado.location_hours.length>=1){
                            hour = '<div class="tripdetail_cell1">Horarios:</div>';
                            for (var h=0;h<resultado.location_hours.length;h++)
                            {
                                hour += '<div class="tripdetail_cell2">'+resultado.location_hours[h].day_a+' a '+resultado.location_hours[h].day_b;
                                hour += ': '+gethour(resultado.location_hours[h].hour_a)+' a '+gethour(resultado.location_hours[h].hour_b)+'</div>';
                            }
                        }
                    }
                        

                }


                for (var m=0;m<resultado.location_services.length;m++)
                {
                    var services_list=resultado.location_services[m].name;

                    services+='<a><div class="tripdetail_cell2">'+services_list+
                    '</div><a>';
                }
                for (var k=0;k<resultado.users.length;k++)
                {

                    var userlist=resultado.users[k].user;
                    users+='<a><div class="tripdetail_cell2">'+userlist+
                    '</div><a>';
                }
                for (var i=0;i<resultado.weather.length;i++)
                {
                    c = (resultado.weather[i].temp-32)*0.5556;
                    weather = '<div class="tripdetail_weather gradeh">';
                    weather +='<p>Clima actual en: '+resultado.activities[i].city_name+'</p>';
                    weather += '<div><img src="http://l.yimg.com/a/i/us/we/52/'+resultado.weather[i].code+'.gif"/></div>'
                    weather += '<p>'+Math.round(c)+'° C, '+resultado.weather[i].text+'</p>';
                    weather += '</div>';
                    
                }

                if(resultado.currency!=''){
                    currency = '<div class="tripdetail_curren">';
                    currency +='<p>'+resultado.currency.to.col0+' <i class="fa fa-arrow-right"></i> '+resultado.currency.from+'</p>';
                    currency += '<p><i class="fa fa-money"></i> '+Math.round(resultado.currency.to.col1*10000)/10000+' '+resultado.currency.to.col0+'</p>';
                    currency += '</div>';
                }

                $('#date').html(date);
                $('#arrival_name').html(name);
                $('#nights').html(nights + "  noches");
                $('#hour').append(hour);
                $('#reservation').html(reservation);
                $('#comments').html(comments);
                $('#service').append(services);
                $('#users_list').append(users);
                $('#weather').append(weather);
                $('#currency').append(currency);

        }

$(document).ready(function() {
    
    var post_token =  window.localStorage["token"];
    var location_id=getParameterByName("location_id");

    if(window.localStorage["offline"]=='Off'){
        $("#saveload").css('display','block');
        $("#state").css('display','block');
        
        $.ajax({
                method: "GET",
                url: "http://turismoexmar.com/api/itineraries_locations/details/" + location_id,
                dataType: "json",
                context: document.body, beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + post_token);
                }
            }).done(function (result) {
                window.localStorage["result_details3"+location_id] = JSON.stringify(result.data);
                start();

            }).fail(function(result){

                $("#saveload").css('display','none');
                $("#state").css('display','none');
                var jsonr = JSON.parse(result.responseText);
                if(jsonr.success==false){
                if(jsonr.data.message='Expired token'){
                    navigator.notification.alert('Favor de volver a ingresar tus credenciales');
                    window.localStorage["token"]='';
                    location.href="index.html";
                }else{
                    navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
                }
            }else{
                    navigator.notification.alert('Ha ocurrido un error, favor de tratar más tarde',null,'error','OK');
                }
            });

    }else{
        $("#saveload").css('display','none');
        $("#state").css('display','none');
        if(window.localStorage["result_details3"+location_id]==''){
            $('#alertoffline').show();
        }else if(!window.localStorage["result_details3"+location_id]){
            $('#alertoffline').show();
        }else{
            $("#saveload").css('display','block');
            $("#state").css('display','block');
            var resultado = JSON.parse(window.localStorage["result_details3"+location_id]);
            for (var i=0;i<resultado.activities.length;i++)
            {
                if(location_id==resultado.activities[i].id){
                    start();
                }else{
                    $('#alertoffline').show(); 
                }
            }
        }
        
    }

    $('#back').click(function(){
        if(device.platform === "iOS" && parseInt(device.version) === 9){
           var resultado = JSON.parse(window.localStorage["result_details3"+location_id]);
            for (var i=0;i<resultado.activities.length;i++)
            {
                itinerary = resultado.activities[i].itinerary_id;
            }
            location.href="travel_list.html?itinerary_id="+itinerary;
        }else{
            navigator.app.backHistory();
        }
    });
   
});
